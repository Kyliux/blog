---
title: Sorry Children
date: 2017-01-30T22:36:49+08:00
author: watashi
cover: https://sorrychildren.com/facebook-en.jpg
categories:
  - interesting
tags:
  - values
  - post
---

Dans quelque temps, le monde n’aura rien à voir avec celui dans lequel nous vivons aujourd’hui. Cette situation critique nous oblige à regarder les choses en face : à moins que nous ayons tout fait pour éviter le pire, nous aurons tous une responsabilité envers nos enfants.
Et nous aurons peut-être envie de nous excuser.
