---
title: "Le coté positif"
cover: /img/ptilogo/brightside.jpg
author: Nicolas
date: 2015-11-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---

# Le trucs cool  !

Ce pays ne peux pas juste avoir autant de problèmes, il doit y avoir des points positifs et vous avez raison de penser ça.


<!--more-->


 En effet en plus de certains endroits naturels encore preservés qui peuvent faire plaisir aux touristes, les businessmans peuvent aussi se régaler. La corruption et la fexibilité de la loi permet aux entrepreneurs de faire énormément de choses comme la zone "grise" des choses possible est énorme. Ici, aucuns standards, aucunes normes, et seulement quelques règles basiques à respecter dans la vie de tous les jours, et le business n'en fait pas exception. Si vous êtes entrepreneurs, tentez votre chance ! Mais n'oubliez pas que vous serez la proie de toutes les arnaques possible en tant qu'étranger !
