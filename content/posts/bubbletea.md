---
title: "Le bubble tea !"
cover: /img/ptilogo/bubbletea.jpg
author: Nicolas
date: 2013-11-11T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Le Bubble Tea ! Ah encore quelque chose que j'ai découvert là bas ! En fait à Taiwan comme dans toute l'Asie en général, il y a des petits magasins de nourritures et boissons PARTOUT.

<!--more-->

Il y a une veritable culture de la nourriture la bas. Dans les temps chaud de l'été, il n'y a rien de tel que de commander après une bonne scéance d'escalade, une bonne quantité de boisson fraiche, fruité et sucrée ! Le dernier ingrédient magique qu'ils ajoutent ce sont les "bubbles" noir qui flottent légèrement dans votre verre. En fait ce n'est rien de plus que des boulettes de farine de riz ( et ne me demandez pas pourquoi la couleur est noire ) C'est à consommer avec modération, autant à cause du sucre qu'à cause des "bubbles" mais on y resiste que difficilement, la moitié des Taiwannais y est addictés !
