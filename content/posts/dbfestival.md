---
title: "Le dragon boat festival"
cover: /img/ptilogo/dragonboat.jpg
author: Nicolas
date: 2013-08-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Le Dragon Boat Festival est l'un des évènements incontournable des chinois avec le nouvel an Chinois.

<!--more-->


C'est un festival qui vient d'une très vieille tradition qui appartient à l'histoire de la Chine. Un jour, un très bon conseillé du roi a mis fin à ses jours en se jettant dans la rivière. Pour le sauver, les habitants ont jetés du riz dans les rivières pour nourrir les poissons afin qu'ils ne mangent pas son corps, et pendant ce temps parcourraient les rivières en bateau pour le retrouver. Cette tradition est restée, et durant cette période en plus des courses de bateau en forme de dragon, on y mange aussi de la nourriture ( que j'adore! ), que l'ont appelle "Zongzi". Idéal dans casse croute à base de riz !!
