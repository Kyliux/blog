---
title: "Le sport à Taiwan"
cover: /img/ptilogo/sport.jpg
author: Nicolas
date: 2013-10-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---



A Taiwan, il y a, contrairement à d'autres région d'Asie, un bon nombre d'installations pour pratiquer du sport.
<!--more-->

 Les plus populaires sont le Badminton, le Hip-Hop, le Basket, le Baseball et le Soccer. Mais on y trouve aussi de l'escalade ! On trouve à Taiwan pas mal de spot vraiment sympa pour escalader indoor comme outdoor et celui que j'ai particulièrement aimé se trouve au bord de la mer: Lóngdòng. (龍洞) Il est génial non seulement pour sa taille et le nombre de voies que l'on peut y faire, mais aussi pour le paysage et la Mer juste a coté pour se jeter dedans ! N'oubliez pas vos T-shirt et la crème solaire, parce qu'une exposition prolongée au soleil là bas peut vraiment vous donner un coup de soleil sevère, c'est l'expérience qui parle.
