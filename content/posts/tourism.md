---
title: "Tourism"
cover: /img/ptilogo/tourismthailand.jpg
author: Nicolas
date: 2016-12-04T21:56:28+01:00
tags :
  - Thailand
  - article
---

La Thailand est un pays touristique qui n'a plus à faire ses preuves. On y trouve partout des installations pour les visiteurs.

<!--more-->


 Que ça soit locations en tous genres ( kayak, scooter, voiture, conducteur, bateau.. ), mais aussi logement, transport et restaurant vous trouverez tout ce dont vous aurez besoin à proximité. Les autorités ont même pris l'habitude de ne pas verifier la présence des différents permis de conduire aux étrangers, le permis étant souvent dernier étant souvent inexistant. Cela dit, c'est aussi un grand pays, et des différences énormes en fonction de l'endroit où vous vous trouvez. Personnellement je vous conseillerais d'éviter Bangkok et d'aller dans des endroits plus reculés dans les montagnes au nord, comme sur les iles paradisiaques au sud.
