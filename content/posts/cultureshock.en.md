---
title: "Le choc culturel"
cover: /img/ptilogo/cultureshock.jpg
author: Nicolas
date: 2015-03-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---


If you plan to live for some time in Indonesia and come from an occidental country, you will probably get in many aspect a cultural shock.

<!--more-->


 You will be the witness a type of society that work in a way really different from yours. When I came there I was very enthusiastic and I was so glad to discover all this news things. But I have to confess that after 6 months living there ( I stayed 14 months ) you get tired of the differences when you don't understand why the things are this way. And finally you will know why people act like that, as you will understand the global picture, and you will start to think that the things are the best they can be according to the situation.
This differences are classified by topic : pauverty, corruption, beliefs, education, people relationships, values, activities, place of work in life, money and environnement. As you can see it cover a lot of things. If you are going to live there one year, you will understand all this differences and that will make you a better person with a whole new open-mind.
