---
title: covoiturage libre
date: 2016-01-31T23:58:15+08:00
author: watashi
cover: http://watashi.fr/img/baniere/covoit.jpg
categories:
  - interesting
tags:
  - values
  - post
---

Covoiturage-libre est une copie libre de Blablacar. C'est une vraie solution de covoiturage efficace et gratuite qui à la place de faire payer le service aux utilisateurs comme Blablacar, renforcera l'entre aide entre les personnes et l'usage d'un système qui n'est pas centralisé.
