---
title: "La nourriture"
cover: /img/ptilogo/food.jpg
author: Nicolas
date: 2015-08-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---

# La nourriture en Indonésie !

Pour la nourriture, le tableau comprend les extrèmes. Dans un pays pauvre où vivent tellement de personnes, vous trouverez de tout.

<!--more-->


A savoir que les aliments de manière individuel ne sont pas industriels et donc ont bien meilleur gout ! Par contre il va falloir se méfier des standars de propreté, des moyens de conservations et de cuisson. Vous trouverez dans les grandes villes, des enseignes internationales que je ne nommerais pas, mais aussi plusieurs type de place pour manger. Vous les trouverez aussi bien en modèle portatif ultra light, qu'en grand format sédentaires. Kaki lima : le model ultra light, souvent engin à roulette au bord de route qui vend de la nourriture de qualité douteuse ; je déconseille vivement. Warung : un endroit qui est construit pour la journée, ou permanent au bord de la route. Peut être un très bon rapport qualité/prix mais se méfier. Depot : pareil qu'un restaurant mais moins luxueux dont moins cher. La qualité devrait être au rendez vous. Généralement, vous pourrez commander votre nourriture des depots sans avoir même à vous deplacer ;).
