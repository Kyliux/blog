---
title: "Traditionnal Chinese"
cover: /img/ptilogo/chinois.jpg
author: Nicolas
date: 2013-11-09T21:56:28+01:00
tags :
  - Taiwan
  - article
---


The Chinese traditional characters are a part of the language imposed in Taiwan.

<!--more-->

 As China changed from traditional to simplified, there is only 3 that are still using this way of writing : Taiwan, Hong-Kong and Macao. These characters are similars to the other but have more strokes, that makes them more complicated, but it doesn't worry foreigner to learn them. With motivation and time you can reach any objectives, besides, you will be the proud ones that are able to read and write with this ancestral symbols that even Chinese people, may not even know ! Furthermore, you will be able to understand the simplified characters easily. This being said you will need to spend two good years to get a good level. But do not worry, Taiwan have very good language center integrated at the university where you will learn Mandarin with a group of 10 people essentially the Japanese.
