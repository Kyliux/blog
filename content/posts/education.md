---
title: "L'Education"
cover: /img/ptilogo/education.jpg
author: Nicolas
date: 2015-05-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---

Il y a une expression en Indonésie "MALAS BRO!" qui se traduit par "j'ai la flemme". La différence c'est qu'en Indonésie cette expression a une conotation qui est presque positive et elle justifie bien des choses dans la vie.

<!--more-->


Aussi ne vous attendez pas à une implication particulièrement forte, ni des élèves, ni des professeurs, ni même des employés de manière générale car les Indonésiens sont devenus des professionnels pour rendre des agendats et règles flexibles. C'est un pays dont la culture est tellement tournée pour éviter les conflits que personne n'osera jamais vous dire non et la seule issue sera de résoudre le problème au plus vite quoi qu'il en coute à long therme. Ce comportement engendre des examens non seulement faciles, mais souvent open-book où les élèves ne se cachent même pas pour tricher. Et une éducation où la triche est si bien acceptée, est très propice au plus grand fléau qui fait des ravages dans ce pays : la Corruption ! A tel point que c'est devenue une notion très vague et globalement acceptée par la population qui ne savent même plus trop ce qui est, ou non, de la corruption. Les pots de vins sont calculés dans tous les budgets.
