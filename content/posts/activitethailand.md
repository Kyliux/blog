---
title: "Activités Thailand"
cover: /img/ptilogo/activitethailand.jpg
author: Nicolas
date: 2016-12-04T21:56:28+01:00
tags :
  - Thailand
  - article
---

En plus des activités sportives çités dans le post "aventures", il existe toutes sortes d'activités plus tranquilles ..

<!--more-->

...  qui plairont aux moins sportifs comme la visite des Temples, des paysages et plages magnifiques à visiter, mais aussi des grand marchés de bangkok, des tours en bateau et je ne parle pas des massages exquis qui vous seront proposés un peu partout. Il y a aussi toutes sortes de show auxquels vous pourrez assister, que ça soit du jonglage enflamé, des danses et chants traditionnels ou les très célèbres performances "ping pong show" que vous pourrez assister autour de la très délicieuse nourriture Thailandaise. Si vous souhaitez être tout à fait posé, à Pukhet se trouve des rues remplient de bar, de filles et d'alcool qui vous tenteront de consumer votre porte feuille.
