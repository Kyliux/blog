---
title: "Memorial parc"
cover: /img/ptilogo/memorialpark.jpg
author: Nicolas
date: 2013-09-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Taiwan in quest to preserve it's past, and keep distinctive from China.


<!--more-->

 Builded a good amount of parc that are called "Memorial Parc" that have each an unique name of important people that changed history. We find there beautiful green places, pond, fishes, tortules, bonsaï, museums and traditional Chinese structures such as arcs and temples ! This places are very calm and great to hang out, read books or simply discourir the Taiwanese history ! It strongly advice drinking bubble tea after this quest
