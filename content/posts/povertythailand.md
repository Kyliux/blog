---
title: "Pauvreté Thailand"
cover: /img/ptilogo/pauvretethailand.jpg
author: Nicolas
date: 2016-12-04T21:56:28+01:00
tags :
  - Thailand
  - article
---

Comparé à l'indonésie, la thailande est un pays bien mieux organisé et ouvert aux touristes.

<!--more-->

 De ce fait, ça en fait un pays bien plus international où l'anglais est bien mieux parlé. Je n'ai eu aucun problème de communication avec qui que ce soit. On n'y remarque pas vraiment la pauvreté, je n'ai pas croisé de mendiants. Je me suis vraiment senti bien entouré et en securité durant tout le long, les personnes sont chaleureuses, souriantes et très respectueuses. La parfaite combinaison pour plaire aux touristes, je n'ai pas eu la chance de me faire de vrais amis Thailandais comme je voyageais beaucoup.
