---
title: "Le choc culturel"
cover: /img/ptilogo/cultureshock.jpg
author: Nicolas
date: 2015-03-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---


S'il vous prend l'envie de vivre quelques temps en Indonésie, vous aurez un choc culturel sur bien des points.

<!--more-->


S'il vous prend l'envie de vivre quelques temps en Indonésie, vous aurez un choc culturel sur bien des points. Vous allez observer une société qui "fonctionne" mais d'une manière complètement différente que la notre, vous allez respectivement commencer par rire des différences, puis les detester en ne comprennant pas la raison de tels agissements, pour finalement voir le tableau global et voir qu'il y a des raisons pour chaques choses, et que c'est peut être le meilleur moyen pour que les choses fonctionnent à cette étape d'évolution de ce pays. Les différences se retrouveront au niveau de la pauvreté, la corruption, des croyances, de l'éducation, de leur modèle, des relations entre les gens, de leur manière de raisonner, de leurs valeurs, de leurs activités, de leur relation avec le travail, de l'argent et l'environnement ( un peu tout quoi ^.^ ) . Toutes ces différences valent la peine d'être cités, et leur ensemble permet une compréhension globale du pays. SI vous allez en Indonésie un an, vous en sorterez changés et bien plus ouvert d'esprit.
