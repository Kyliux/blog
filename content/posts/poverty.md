---
title: "La pauvreté"
cover: /img/ptilogo/poverty.jpg
author: Nicolas
date: 2015-06-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---


La pauvreté, l'éducation, la religion et la corruption représentent les principales influences des individus qui, confrontontés à la société vont forger des valeurs communes à toute la population.

<!--more-->


Ces valeurs seront un socle de base pour toute la couche relationnel de la société. Il va donc définir ce qui se fait, ou ne se fait pas. Les Indonésiens ont une culture où on place une grande valeur dans la famille et son extention : les amis. Ils vont donc chercher le bonheur mais aussi le soutient de leur pairs. Ce comportement a tendance à, lorsqu'on fait face à un problème, à ne pas chercher la faute chez un individu qui, par définition, fait partie de la famille. Ainsi ce qui est le plus mal vu en Indonesie, c'est de faire perdre "la face" à quelqu'un ( en lui criant dessus par exemple face à ses pairs ). A contrario, répondre de manière positive et suivre le groupe est essentiel et si jamais il vous arrive de vouloir contredire quelqu'un, il faudra lui parler entre quatres yeux, tourner autour du pot jusqu'à ce qu'il comprenne le sens de votre discours. Ainsi d'une part, certains vont profiter de la situation pour s'en sortir indemme malgrés leurs tords en se cachant derrière le groupe, quand à d'autres, vont profiter du groupe en utilisant la majorité pour faire face aux tiers. On distingue donc deux extrèmes : Pour les relations commerciales, de la corruption ou du vol plus ou moin légal. Pour les relations d'amitités, des moments vraiment extraordinaires avec les Indonésiens. Aussi je vous conseille d'abord d'être de vrais amis avec eux avant d'entreprendre quoi que ce soit.
