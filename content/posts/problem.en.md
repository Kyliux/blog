---
title: "Problems"
cover: /img/ptilogo/problem.jpg
author: Nicolas
date: 2015-09-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---

# Les challenges !


As you can see, Indonesia is still facing many problem that are linked with poverty.


<!--more-->

 We can mention the international pressure and investment that create layers of corruption that ruins the take off of the country. The leak of good education that is required to share the vision and the benefits from a corrupt-free country. The high born rate that doesn't let the infrastructure to follow the population growth and great inefficency and pollution in big cities. To finish with the religion that promote passivity and irresponsibility in their acts. The multi-religion policy of Indonesia is a great challenge as well, just as the conflicts between Chinese born Indonesian and Indonesians. Talking about religion, the multi-religion policy of Indonesia is a great challenge as well, just as the conflicts between Chinese born Indonesian and Indonesians. There are as well some natural disaster happening in Indonesia were some people are burning the forest in the north of Indonesia, that is not only killing a massive amount of animal to create soja soil plantation, but they are polluting the air in Singapore and the surrounding countries too.
