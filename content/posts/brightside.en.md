---
title: "The bright side"
cover: /img/ptilogo/brightside.jpg
author: Nicolas
date: 2015-11-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---



But this country can not only have all this problems, there should be some positif points and you may be right to think this way.


<!--more-->


 Indeed, some natural resources are still very preserved from pollution and tourism and we will hope that this country will keep it this way.. The corruption and the flexibility of the law make everything possible in this country as the grey zone is very wide. Here almost no norms and only very basic rules to take care of, the business is not an exception. If I were you, I would take care of the relationship between you and the Indonesian that you will associate with, because you will not be allowed to start something alone. If you are an entrepreneur, just take your chance, scale the market, mesure your risk, and avoid all the scams that are awaiting the foreign investment there.
