---
title: Karanta stage experience
date: 2017-06-03T14:21:26+08:00
author: Nicolas
cover: /img/karanta.png
categories:
  -
tags:
  - poem
---

Le poeme suivant à été écrit suite à une de mes expériences de stage.

<!--more-->

### Stagiaire en start-up


Karanta était pour moi l'opportunité rêvée
Pour que dans mon cursus je puisse combiner.
Mes deux domaines de specialités.

On y apprend la vie en entreprise,
Celle qu'on aime, qu'on méprise.

Cette expérience m'a apporté.
Une meilleur vision de l'amitié.

Celle qui porte en son sein
La force d'un objectif commun

Chacun aura alors l'opportunité
D'affirmer ses capacités

De Partager son temps, ses connaissances
Comme un parent, a toute l'assistance

Pour qu'à terme l'entreprise puisse beneficier,
D'une seine et durable pérennité.

Mais gardez à l'esprit votre position
Qu'on ne vous prenne pas pour un pion

Car une fois votre temps écoulé
Ce n'est pas dit que vous pourrez restez.

Ce n'est pas de promesse, qu'il faudra se nourrir
Même si leurs caresses, sauront vous divertir.
