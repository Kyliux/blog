---
title: El mate !
date: 2016-10-29T09:30:49+08:00
author: Nicolas
cover: /img/bebemate.png
categories:
  - argentina
tags:
  - poem
---

Autour de vous, vous partagerez
A tout moment de la journée
La béatitude ensoleillée
Que vous procure le maté

<!--more-->
