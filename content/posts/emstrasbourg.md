---
title: EM Strasbourg experience
date: 2016-06-03T14:21:26+08:00
author: Nicolas
cover: /img/emstrasbourg.png
categories:
  -
tags:
  - poem
---

Le poeme suivant à été écrit suite à mon expérience à l'EM Strasbourg

<!--more-->

### Experience EM Strasbourg

La critique est facile.
Et c'est sans prétentions.
Que je vais vous faire part
A vous, de leur prestation.

Une école comme une autre
Mais ce serait sans recevoir.
Les graines envolées
D'une experience notoire.
