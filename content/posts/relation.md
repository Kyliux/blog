---
title: "Les liens avec les étrangers"
cover: /img/ptilogo/rforeigners.jpg
author: Nicolas
date: 2013-11-19T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Comme dans tous les pays les étrangers ont une place particulière dans la société, une place pleine de préjugés historiques. Celle-ci change en fonction de la classe sociale dont on fait partie et dans quelle sorte de pays on se trouve.

<!--more-->

Taiwan ne fait pas exeption, et étant un pays qui attire plus d'expatriés que d'immigrants ( vous sentez la nuance ?), ce pays contient une partie de personne qui voient encore les étrangers "blanc" comme les riches américains qui vient investir et "profiter" des Taiwanais. Il sera donc, très prisé par les filles et le bienvenue dans toutes sortes d'évènements pour son exotisme ( relatif ), aussi bien qu'invité à rentrer chez lui comme concidéré comme un profiteur malveillant. Il est vrai que certains jeunes d'angleterre que j'y ai croisé m'ont fait honte(...), irrespecteux et profiteur au possible. Ils alimentent allègrement les journaux locaux qui portent un attrait tout particulier pour les étrangers. Mais heureusement que ce n'est pas une généralité. Les expatriés, souvent plus éduqués et respectueux que leurs pairs, peuvent jouir de l'hospitalité et de la gentiesse des Taiwanais pour qui les valeurs de la vie sont le bonheur et le partage, et feront tout pour vous aider à découvrir leur culture. 
