---
title: "How to spend free time as Indonesian"
cover: /img/ptilogo/activity.jpg
author: Nicolas
date: 2015-07-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---


After some weeks in Indonesia, I felt kind of bored because from my point of view, there was nothing to do. So I asked the people in my university, the locals to know what they do during their free time, the answer was terrible to me.

<!--more-->


 They simply answered me that if they aren't spending time in their home with their family, they would just go to the shopping mall, do some shopping, watch a movie and come back shopping. The only explication I found that would make the mall so attractive is the free air-conditionned there and not having the parents saying what they should or not do. To sum up, they don't like so much sports and heat outside is a problem for many activities. They do not like do travel in far places, but can make exception for a great restaurant as far as 3 hours driving.
