---
title: "En résumé"
cover: /img/ptilogo/overall.jpg
author: Nicolas
date: 2015-12-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---


Comme je l'ai expliqué, ce pays est encore en cours de développement et il y a encore beaucoup de choses à y faire que vous soyez un toursite ou un entrepreneur.

<!--more-->


Pour profiter le plus de votre séjour, et ne pas être considéré comme un porte feuille sur pattes, je vous conseille vivement de vous joindre à la population en vous faisant de vrais amis. Si votre séjour est long, je vous conseille de venir les yeux grands ouvert, sans idées préconçues et de garder une grande flexibilité au quotidient dans tous les domaines. S'il y a quelque chose que les indonésiens peuvent nous apprendre, c'est de ne pas vivre pour travailler, et que les relations humaines sont importantes.
