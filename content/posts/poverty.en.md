---
title: "La pauvreté"
cover: /img/ptilogo/poverty.jpg
author: Nicolas
date: 2015-06-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---

Poverty, education, religion and corruption are the main cause that influence individuals behavior in the society. 

<!--more-->


The relational culture of the society will evolve according to this characteristics and define what can be, or not be done. In the Indonesian culture, family and its extension, the friends are the most important thing. They will find support and happiness with them. This behaviour will result in the creation of a group responsability. When some mistake is made, it will be very unlikely to find the real responsible but more facing a group uncomfortable moment when everyone protect everyone. There is something in Indonesia that is terrible, it's the "losing face" fear ( when you scream at someone with his pairs ). A contrario, you should speak positively facing group of people and if you may need to speak to someone about a mistake he has made, you should be alone with him and walk-around the subject to make him understand what you really means. This situation will make stronger the position of weak people that will be able to escape problems easier than other culture, but in other hands, that will also give them a second chance. In a nutshell, I would say that there are two extremes, in one hand pauverty made Indonesian have "bad" behaviour when you want to make business with them as a white guy because they will be more likely to protect each other while subject to corruption and laziness. But in the other hand, the Friendship you get in Indonesia can be extraordinary because they would be so happy having a foreigner friend. For this reason I advice you to make friend before starting any business there
