---
title: "Hip-hop in Taiwan"
cover: /img/ptilogo/hiphop.jpg
author: Nicolas
date: 2013-11-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---


It's amazing how the HIP-HOP spread in Taiwan.

<!--more-->

It's even more obvious when you see the TV advertisements that promote speakers specialized in this street activity. I have seen nowhere else in the world ! In Taipei, you can find tons of dance school practicing in all the free places in the capital. We can mention the Memorial-Parcs, places protected by the rain such as the temples or even under bridges. Nothing can stop them to share and improve their skills. This now does belong to the street-art culture in Taiwan ! I have seen some of their show and i can tell you that it's very impressive ! Great organisation, choreography, scenery and disguises with themes, as well as the amazing number of club that are practising, it's crazy !
