---
title: "Nourriture Thailand"
cover: /img/ptilogo/nourriturethailand.jpg
author: Nicolas
date: 2016-12-05T21:56:28+01:00
tags :
  - Thailand
  - article
---

Ah la nourriture Thailandaise me manque déjà.

<!--more-->

 Les fruits, légumes, viandes et jus qu'on y déguste sont vraiment exellent et bon prix. J'y retournerais rien que pour en faire un voyage culinaire. Il y a beaucoups de mélanges riz/pâtes avec des fruits dans les plats que vous trouverez. Et contrairement à ce que vous pouvez imaginer ce n'est souvent pas épicé ! Vous y mangerez pour quelques euros un peu partout sans avoir besoin de vous inquiéter de la qualité de la nourriture !
