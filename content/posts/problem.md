---
title: "Les souçis"
cover: /img/ptilogo/problem.jpg
author: Nicolas
date: 2015-09-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---

# Les challenges !

Vous l'aurez compris, l'indonésie est un pays qui doit faire face à bon nombres de problèmes.


<!--more-->


 Ils sont tous liés à la pauvreté et on notera bien évidemment la corruption à tous les niveaux qui ruine l'évolution du pays. En effet, que ça soit le policier qui va pour quelques brides être influencé pour designer un coupable, ou un ministre qui changera subitement de déçision suite à quelques billets ou service bien placés : Personne n'est épargné et le pire c'est que ça fait partie maintenant de leur culture. Les conflits d'interêts entre les minorités éthniques nés sur le territoire ( Chinois/Indonésiens ) , religieuse ( principalement Musulmane et Chrétien ) et sexuelles n'arrangent pas non plus les choses.
