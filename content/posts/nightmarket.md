---
title: "Les nightmarkets"
cover: /img/ptilogo/nightmarket.jpg
author: Nicolas
date: 2013-07-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---

# Les night markets.

A Taipei, vous trouverez la nuit des "Night market". Comme son nom l'indique, il s'agit de marchés de nuit dans lesquels se retrouvent toutes les tranches d'âge de la population autour d'un repas.

<!--more-->


Vous y trouverez une rue bondée de monde avec sur chaques trottoirs des personnes vendants toutes sortes de choses, et principalement de la nourriture. Je recommande fortement d'aller pour gouter les jus locaux, les fruits ( tentez le Durian! ) mais aussi le très célèbre "stinky tofu" très apprécié par les Taiwanais. La plupart des plats sont cuis et cuisinés devant vous et comme les Asiatiques adorent cela, la plupart des plats sont fris et assez gras, mais c'est tellement bon ! J'en garde toujours un très bon souvenir, et l'ambiance traditionnelle Asiatique est assurée !
