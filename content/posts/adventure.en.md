---
title: "Aventure"
cover: /img/ptilogo/aventure.jpg
author: Nicolas
date: 2016-12-04T21:56:28+01:00
tags :
  - Thailand
  - article
---

Thailand will offer you all kinds of activities in very competitive prices compared to the rest of the world.

<!--more-->

 I have particulary appreciated the open water certificate in Koh Tao, my first sceance of kite-surf in Koh-phangan, the trekking in Chang Mai, the nights with open skies, the visit of traditional villages, the bamboo rafting and the elephants in Chang Mai. Without speaking about the uncountable scooter trip to visit around all the places I have been. This being said, there are things to take care of, for example, how they treat their elephants, the drugs they give to the tigers to hug and the manner they respect local population. All national parc are quite expensive, the policies will not like to see you coming with a tent..
