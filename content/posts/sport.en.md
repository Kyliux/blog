---
title: "Sport in Taiwan"
cover: /img/ptilogo/sport.jpg
author: Nicolas
date: 2013-10-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---



While in some other Asian countries you may have to struggle finding sport facilities.

<!--more-->

Taiwan have a good culture to practice sports, you will meet your needs. The most populars are the Badminton, Hip-Hop, Basketball, Baseball and the Soccer. You will find as well climbing walls ! You can find really good spot in Taiwan, indoor as well as outdoor and the one I love the most is near by the sea: Lóngdòng. (龍洞) It is a great place not only because of it's size, and the number of the paths you can climb on, but as well for the landscape it offers and the sea that is just waiting you to got inside and take a swim. Do not forget your clothes and your solar creme because long exposure to the sun will give serious damages to your skin. I discovered it by my expense.
