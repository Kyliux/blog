---
title: "Les activités"
cover: /img/ptilogo/activity.jpg
author: Nicolas
date: 2015-07-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---

# Les activités en Indonésie !

Après quelques semaines, et à défaut d'avoir trouvé une réponse satisfaisante moi même, je demande à mes camarades Indonésiens quels activités extra-scolaires ils font, et la réponse m'a scotché.

<!--more-->


 Ils m'ont gentillement expliqué que s'ils ne "glandent" pas chez eux, ils vont soit au cinéma, soit aux Malls faire du shopping. La réponse est invariable. La seule explication que j'ai trouvé, c'est que les Malls sont les seuls endroits où il y a de la clim et qu'ils sont libre de flirter sans avoir leur parents sur le dos. En somme, ils ne sont pas très sportifs et la chaleur est le frein pour bien des activités. 
