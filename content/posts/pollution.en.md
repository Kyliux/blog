---
title: "Pollution"
cover: /img/ptilogo/pollution.jpg
author: Nicolas
date: 2013-11-15T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Taipei, is an example for many asian cities if you focus in the transports and the relationship with the trash.

<!--more-->

 For the transport, the city not only invested in the very efficient Japanese Mass Rural Transport, but it also allow the cyclist to take over the center of the city when they installed free rental of bikes. That makes Taiwan one of the Asian capital that aren't that subject to traffic jam. In Taiwan there is also a great way to deal with the trash, every thursday in the early morning, a truck going though all the street and it's the citizen themself that are filling the truck bin. It's not only adding some neighbor dynamism but it educate and implicate the population in the recycling process !
