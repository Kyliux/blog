---
title: "Taiwan summary"
cover: /img/ptilogo/overallimpression.jpg
author: Nicolas
date: 2013-12-19T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Taipei is what you can expect from a modern capital city in Asia.

<!--more-->

 This city still have some deep traditional roots but for it's capital title, it is also a city open to the world. Traditional because you can still find there are still nowadays tribes that lives near by the mountains, because of the food and the Chinese characters, But also international because it get international investment, influence, import/export. Do not forget it's other name as the Chinese tiger ! The fact that it's a small island, reduce its air pollution, makes the sea closer as well as the mountain.
