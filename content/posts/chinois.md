---
title: "Le chinois traditionnel"
cover: /img/ptilogo/chinois.jpg
author: Nicolas
date: 2013-11-09T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Les charactères Chinois traditionnels font partie de la langue imposée à Taiwan.
<!--more-->

La Chine étant passée du coté des charactères simplifiés, il ne reste que 3 pays qui utilisent encore ces symbole : Taiwan, Hong-Kong et Macao. Ces charactères sont constitués de plus de traits, ce qui les rend plus compliqués, mais cela ne devrait pas effrayer les occidentaux à les apprendre. En effet avec de la motivation on arrive à tout, et la fiereté de savoir lire et écrire les charactères ancestraux est bien plus valorisant que savoir simplement les caractères que tous les chinois connaissent déjà ! En plus de ça, vous saurez aussi lire les caractères simplifiés comme ils sont "semblable" on peut facilement deviner leur signification dans le contexte. En plus de ça, Taiwan propose de très bon centre de langues intégrés aux écoles où vous pourrez apprendre la langue de manière quotidienne dans des groupes de personnes jeunes ou moins jeunes composés principalement de Japonnais.
