---
title: Velorution Strasbourg
date: 2020-01-30T21:24:06+08:00
author: Nicolas
cover: /img/velorution.jpg
categories:
  - france
tags:
  - website
  - velorution
  - engagement
---

C'est l'histoire d'un jeune homme, frappé par la véracité de ce slogan, entendu par hasard dans une rue Strasbourgeoise.

<!--more-->


### Libérez les cyclistes, enfermés dans les voitures


C'est l'histoire d'un jeune homme, frappé par la véracité de ce slogan, entendu par hasard dans une rue Strasbourgeoise. Par hasard ? Non, quand toute la France est occupée par l'industrialo voituri, il aimait croire que cette petite Métropole était la meilleure ville de France pour les cyclistes. Mais ça, c'était avant d'y habiter et de constater que ce n'est pas si rose. En effet, dire que Strasbourg est le meilleur coin de l'enfer ne la rend pas plus attrayante. En effet, en vue des mentalités archaïques de certains décideurs Strasbourgeois pro-voiture qui pensent que la route est une jungle où il faut la plus grosse pour en sortir vivant et montrer qu'on a réussi dans la vie. #OKBOOMER Le collectif de Vélorution Strasbourg a encore bien des choses à dire. N'hésitez pas à vous aussi faire entendre votre voix !
