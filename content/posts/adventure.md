---
title: "Aventure"
cover: /img/ptilogo/aventure.jpg
author: Nicolas
date: 2016-12-04T21:56:28+01:00
tags :
  - Thailand
  - article
---

La Thailande vous offrira toutes sortes d'activités à des prix très compétitif comparé au reste du monde. 

<!--more-->

J'ai particulièrement appreçier le certificat de "open water" à Koh Tao, ma première scéance de kite surfing à Koh phangan, le trekking, la nuit à la belle étoile, la visite de villages traditionnels, le bamboo rafting et les éléphants à Chang Mai, sans parler des innombrables tours en scooter pour visiter les alentours des divers destinations où je suis allé ! Il y a cependant certaines choses dont il vous faudra être regardant, par exemple la manière dont ils traitent les éléphants, les drogues qu'ils donnent aux tigres pour que vous puissiez les carresser et le respect de la population locale. Le prix des parc naturel est plus élevé, et les policiers n'aimeront pas vous voir venir avec une tente pour faire votre propre trip mais pour ce que j'ai entendu, ça se fait !
