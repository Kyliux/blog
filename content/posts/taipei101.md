---
title: "La tour Taipei101"
cover: /img/ptilogo/taipei.jpg
author: Nicolas
date: 2013-06-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---

# La tour de Taiwan

Taipei101 pour son nombre d'étages, surplombe toute la ville et est de ce fait incontournable.

<!--more-->


 Elle vous servira certainement comme point de repère quand vous marcherez en ville, et ce, de jour comme de nuit.
Elle porte chaque nuits des couleurs différentes et il arrive qu'on la compare à l'oeil de Sauron à cause du sommet qui brille couleur flamme. C'est un lieu que j'encourage vivement à visiter à la tombée de la nuit pour profiter des deux types de panorama qu'elle propose.
