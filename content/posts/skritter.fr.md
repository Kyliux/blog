---
title: Skritter app
date: 2020-02-15T21:24:06+08:00
author: Nicolas
cover: /img/skritter-devices.png
categories:
  - chinois
tags:
  - langues
  - chinois
  - apprentissage
---

Petit suivi de l'apprentissage de la langue chinoise avec cette application.

Visit the website : [Skritter](https://www.skritter.com)

<!--more-->


### Statistiques
