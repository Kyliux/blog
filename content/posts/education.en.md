---
title: "L'Education"
cover: /img/ptilogo/education.jpg
author: Nicolas
date: 2015-05-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---

There is an Indonesian expression, that is "MALAS BRO!" that can be translated by "I am lazy now my friend"...

<!--more-->


There is an Indonesian expression, that is "MALAS BRO!" that can be translated by "I am lazy now my friend". The difference is that this expression have a connotation almost good and it justify many things in life. For this reason, do not expect a great implication not from the students but neither from the teacher and that does apply for the employee as well. Many Indonesians became professionals to make the agendas and rules very flexible. It's a country where the culture is oriented to avoid the conflicts, none will never want to tell you "don't do that" or denouce an act of corruption. This behaviour leads to the creation of lazy young people that will only get easy, open-book exams were student doesn't even hide to cheat. If cheating the young age is to easy and almost promoted, it will make great ravages in the adult age where the corruption is seen as a normal way to get extra-money ! The tips are calculated in the budgets to create bridges, and people doesn't even know anymore what corruption means. They is even an to explain them what are acceptable business behaviors.
