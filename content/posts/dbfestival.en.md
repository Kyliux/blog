---
title: "Dragon boat festival"
cover: /img/ptilogo/dragonboat.jpg
author: Nicolas
date: 2013-08-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---


The Dragon Boat Festival is one of the most awaited Chinese event that take place ones a year.

<!--more-->


 It comes directly from a very old tradition that belongs to Chinese history. Once upon a time, there was a loved and very good king adviser. He was devoted and had great honor working in his position, the day he lost the trust of the king, he committed suicide drawing in the river. To save it's body, the locals threw rice in the river to feed the fishes that, instead of eating the body, would rather eat the rice. The objectif was to find the body the fastest possible. This tradition stayed, in this time of the year, there are around China and Taiwan Dragon Boat competition. This tradition also affect the food being eaten during the event. This food is called "Zongzi" (and I Really like it!). It's the best sticky rice based recipe to take away !
