---
title: "I love Indonesia"
cover: /img/ptilogo/tourism.jpg
author: Nicolas
date: 2015-02-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---


## I love indonesia

L'indonésie est beaucoup vu comme un pays préservé du tourisme comparé à la Thailande, de mon avis je dirais que ce serait voir les choses de manière très simpliste.

<!--more-->


Ces deux pays sont tellement grand qu'il y a toujours des endroits paradisiaques où personne n'a jamais mis les pieds, on trouvera aussi des endroits bondés de touristes qui suivent les "tours guide". La différence c'est qu'en Indonésie il n'y a pas tant d'endroits où c'est facile d'y visiter. Le tourisme de masse est concentré aux alentours de Bali, et si vous voulez aller ailleurs, ce ne sera pas si facile et la langue sera la première barrière. Dans la section photo, vous trouverez certains noms de lieu que j'ai visité. Certains endroits ne sont pas facile d'accès, ainsi parler Indonésien et connaitre des gens facilite bcps les choses.
