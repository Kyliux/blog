---
title: "Nightmarkets"
cover: /img/ptilogo/nightmarket.jpg
author: Nicolas
date: 2013-07-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---


In Taipei, you will find a lot of "Night markets". You will find there all the generation of Taiwaneses sharing a meal in the hot street of the capital.

<!--more-->


 You will find there tons of people selling all kind of things but mainly food. I advice you to try there all the kinds of local juice they offer, the fruits (try Durian! ) but also the very famous "stinky tofu" that is trendy in Taiwan. Almost all the food is cooked in front of you and as it's the tradition in Asia, when it's not a soup, they will more likely fry your food. It's time to forget your diet efforts, it's so good !
