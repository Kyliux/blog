---
title: "L'influence de la Religion"
cover: /img/ptilogo/religion.jpg
author: Nicolas
date: 2015-04-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---


La religion prend un rôle majeur dans la vie en Indonésie. Elle représente un champ obligatoire sur la carte d'identité et sur tous les papiers adiministratifs indonésiens.

<!--more-->


Je suis non croyant, j'ai le sentiment que l'église et la mosque représente autant dans ce pays un lieu de rencontre, qu'un lieu de culte où Dieu donne aux "fidèles" un modèle de vie, des valeurs et de la confiance pour leurs futurs respectifs en invoquant des mots comme le "destin". Les rassurer et conforter est une bonne chose comme cela va promouvoir la paix, mais le problème avec le destin glorieux, c'est qu'il promouvoit l'inaction, une vision à cour terme sans efforts nécessaires pour évoluer. Ainsi c'est un cercle vicieux. Une vision à court terme et un manque de prise d'initiatives va créer des personnes passives et incompétentes qui n'évolueront pas. D'autant plus que dans le modèle que promouvoit l'église est catastrophique : une famille heureuse, est un couple marié dès 24 ans avec plein d'enfants ! Avec le confort de l'eglise, le manque d'anticipation financière et peut être aussi de contraception, tout cela va créer des familles nombreuses avec des parents qui n'ont pas les moyens de s'occuper, de nourrir et d'éduquer leurs enfants qui seront donc pauvre à leur tour.
