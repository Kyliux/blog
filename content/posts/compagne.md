---
title: Interactions au 21em siècle
date: 2017-12-03T14:21:26+08:00
author: Nicolas
cover: /img/campagne.jpg
categories:
  -
tags:
  - engagement
  - poem
---

Le poeme suivant à été écrit suite aux panneaux de sensibilisation du gouvernement.

<!--more-->

### Un société d'enfants


Pour moi c'est clair, cette sensibilisation a eu un effet foudroyant.
Une toute nouvelle manière de vivre dans une société d’enfants.

Quand une fille marchera devant moi, dans la même direction,
C’est pas comme si j’avais en moi, quelque chose à son intention.
Je changerais de trottoir, pour pas qu’elle se sente suivie.
Même si dans le noir, d’autres que moi, en auraient peut être envie.

Quelle belle manière de vivre, on serait tellement unis.
A se croiser sans se voir, jours après jours, dans ces transports infinis.

A défaut d’écouter, des conversations charmantes et argumentées,
Où diverses ethnies et générations vont se mélanger.
Je vais pas oser regarder, dans ces « communs » transports,
Des gens et leur intimité, à défaut du paysage, je regarderais Tinder.

Je vais plutôt m’asseoir, à contempler mes souvenirs,
D’une époque dérisoire, où l’on pouvait encore dire.
En quête d’une histoire, avec une certaine innocence,
"Bonjour Mademoiselle, on fait connaissance ? »

Grace à ton iPod et à tout ton vernis,
T’auras un look à la mode, et sur Facebook plein d’amis.
Grâce au numérique, on peut maintenant les éviter.
C’est bien plus pratique, avec le bouton « bloquer ».

Tout cela, vous pouvez aussi dans la vie.
Poliment le dire à quelqu’un sans ennuis.
Je suis très choqué de voir vers où vont les choses.
Car ce n’est pas ces affiches qui changeront quelque chose.
