---
title: "La pollution"
cover: /img/ptilogo/pollution.jpg
author: Nicolas
date: 2013-11-15T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Taipei, est un modèle pour bien des villes asiatiques vi à vis de sa manière à gerer le transport de ces habitants.

<!--more-->

Non seulement cette ville a investis dans un système de transport en commun très efficace, le MRT, mais elle a aussi permis aux cyclistes de prendre des vélos disposés à des bornes, de manière gratuite tant qu'on le temps de leur usage ne dépasse pas les 30min. Il suffit pour ne pas payer de le déposer dans une autre borne. Et ça permet le transport très facile de la population au centre ville qui, contrairement à beaucoup de villes asiatiques. N'a que très peu de bouchons. A Taiwan se trouve aussi un système à taille humaine pour les déchets, tous les jeudi matin très tôt, un camion passe pour les récuperer. Et ce n'est pas les "eboueurs" qui prennent les sachets, mais plutôt tout les habitants qui se déplacent dans la rue pour y déposer les déchets. Non seulement ça ajoute une dynamique rurale, mais ça éduque la population en les implicant directement au processus de recyclage ! 
