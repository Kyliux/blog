---
title: "En résumé concernant Taiwan"
cover: /img/ptilogo/overallimpression.jpg
author: Nicolas
date: 2013-12-19T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Taipei est ce que l'on peut attendre d'une ville du monde asiatique. Celle-ci porte encore des racines traditionnelles profondes tout en s'ouvrant vers une culture internationale.

<!--more-->

Traditionnelle de part l'existance encore aujourd'hui de tribues qui vivent près des montagnes, de la nourriture et des caractères chinois traditionnels. Mais internationle aussi avec sa dépendance au même titre que le japon des investissements étrangers, de l'import/export ainsi que sa place dans le monde, n'oublions pas que Taiwan est le tigre chinois ! On y trouve un aspect bien plus charmant que les grandes villes de Chine grace à la mer, aux montagnes et une relative moindre pollution. Et les personnes qui y vivent sont amicales et pleines de bonnes intentions. Information diverses : Se nourrir en prennnant de la nourriture avec le poignet et les baguettes retournées ça porte malheur. Les mots avec la prononciation "sih" sont à éviter dès que possible comme sa prononciation signifie "mort". Il faudra donc eviter les chiffres avec un 4 dedans, il n'y a pas d'étage 4 en chine. Il faudra aussi ne pas offrir des cadeaux en nombre de 4 etc.. Ne pas offrir de montre non plus, ça implique que tu souhaites la mort de la personne. Les chinois brulent des choses comme du papier argent, des habits et des voitures jouets pour que leurs ancètres puissent les utiliser dans l'autre monde. Quand une fille se marie, elle n'appartient plus à sa famille, mais a la famille de son mari. 
