---
title: "Mémorial parc"
cover: /img/ptilogo/memorialpark.jpg
author: Nicolas
date: 2013-09-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---


Taiwan en quête préservation de son passé, a construit à Taipei bon nombre de parc qu'on appelle des "Memorial Parc" qui portent chacun leur propre nom.

<!--more-->

Ces parcs en plus de leur devoir de mémoire, sont là pour honorer ces personnalités importantes du passé. On y trouve de la verdure, des étangs, poissons, tortues, bonsaï, musées et structures chinoises traditionnelles comme des temples et arches ! Ce sont de bon endroits calmes pour se balader, se poser lire un livre ou tout simplement découvrir un peu plus sur la culture Taiwanaise ! C'est aussi un des endroits où je vous recommande d'aller avec un jus de Bubble Tea bien frais !
