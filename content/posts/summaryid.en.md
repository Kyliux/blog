---
title: "Overall impression"
cover: /img/ptilogo/overall.jpg
author: Nicolas
date: 2015-12-04T21:56:28+01:00
tags :
  - Indonesia
  - article
---

As I have explained earlier, this country is still a developping one and there is still many things to do whenever you are a tourist or an entrepreneur.

<!--more-->


 To enjoy completely your time there, and not be considered as a wallet on foot, I strongly advice you to join at the population and make yourself good friend that will not only help you to learn the language but could become some business partner in the future ! You should keep in mind when you arrive there, that you have to take the things are they are, and your culture won't change it. So keep your open-mind and open-arms to accept all the difference you will find and do your best to understand them. If there is something that Indonesia can teach you, it is that it's worthless living to work, and that humain connection are very importants.
