---
title: "Le Hip-hop à Taiwan"
cover: /img/ptilogo/hiphop.jpg
author: Nicolas
date: 2013-11-04T21:56:28+01:00
tags :
  - Taiwan
  - article
---


C'est très impressionnant la manière dont le HIP-HOP s'est répandue à Taiwan.
<!--more-->

 On voit de la publicité à la télé qui promouvoient des chaines HIFI spécialisées pour cette pratique dans la rue. A Taipei, on voit des écoles de danse un peu partout et surtout une majorité de jeunes le soir qui sortent dans les parcs et à l'abri des temples pour se retrouver et s'initier/perfectionner dans cette danse urbaine. Si bien que cette danse appartient à la culture des jeunes de cette capitale ! Pour avoir assisté à certains de leur show, je peux vous dire que c'est très impressionnant : il y a une sacré organisation avec la chorégraphie à theme, le décor et les déguisements complexe ainsi que la quantité de club qui y participent, c'est fou ! 
